package nl.ordina.ebms.admin;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.ClassResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import nl.ordina.ebms.admin.service.cpa.CpasView;

/**
 * This UI is the application entry point. A UI may either represent a browser window (or tab) or some part of a html page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be overridden to add component to the user interface and initialize
 * non-component functionality.
 */
@Theme("valo")
@SpringUI
@SpringViewDisplay
public class BaseUI extends UI implements ViewDisplay
{
	private static final long serialVersionUID = 1L;
	private Panel springViewDisplay;

	@Override
	protected void init(VaadinRequest vaadinRequest)
	{
		final VerticalLayout rootLayout = createRootLayout();
    createSpringViewDisplay();
		rootLayout.addComponents(createHorizontalLayout(createImage(),createMenuBar()),springViewDisplay);
		setContent(rootLayout);
	}

	private VerticalLayout createRootLayout()
	{
		VerticalLayout result = new VerticalLayout();
		result.setMargin(true);
		result.setSpacing(true);
		return result;
	}

	private void createSpringViewDisplay()
	{
		springViewDisplay = new Panel();
    springViewDisplay.setSizeFull();
    springViewDisplay.addStyleName(ValoTheme.PANEL_BORDERLESS);
	}

	private Component createImage()
	{
		return new Image(null,new ClassResource("/ebms-admin.gif"));
	}

	private MenuBar createMenuBar()
	{
		MenuBar result = new MenuBar();

		result.addItem("Home",event -> getUI().getNavigator().navigateTo(HomeView.VIEW_NAME));

		MenuItem cpaService = result.addItem("CPA Service",null);
		cpaService.addItem("CPAs",event -> getUI().getNavigator().navigateTo(CpasView.VIEW_NAME));

		MenuItem messageService = result.addItem("Message Service",null);
		messageService.addItem("Ping",null);
		messageService.addItem("Messages",null);
		messageService.addItem("Send Message",null);
		messageService.addItem("Message Status",null);

		MenuItem advanced = result.addItem("Advanced",null);
		advanced.addItem("Traffic",null);
		advanced.addItem("Traffic Chart",null);
		advanced.addSeparator();
		advanced.addItem("CPAs",null);
		advanced.addItem("Messages",null);

		MenuItem configuration = result.addItem("Configuration",null);
		configuration.addItem("EbMS Admin Properties",null);
		configuration.addItem("EbMS Adapter Properties",null);

		result.addItem("About",null);

		return result;
	}

	private GridLayout createHorizontalLayout(Component image, Component menuBar)
	{
		GridLayout result = new GridLayout(2,1);
		result.setSizeFull();
		result.addComponent(image,0,0);
		result.addComponent(menuBar,1,0);
		result.setComponentAlignment(menuBar,Alignment.MIDDLE_RIGHT);
		return result;
	}

	@Override
	public void showView(View view)
	{
		springViewDisplay.setContent((Component)view);
	}

}
